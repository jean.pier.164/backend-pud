/* eslint-disable require-atomic-updates */
module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
    return async context => {
      const { params } = context;

      const token =
        params.query[context.app.settings.authentication.apiKey.urlParam] ||
        params.headers[context.app.settings.authentication.apiKey.header];
  
      if (token && params.provider && !params.authentication) {
        context.params = {
          ...params,
          authentication: {
            strategy: 'apiKey',
            token
          }
        };
      }
  
      return context;
    };
  };