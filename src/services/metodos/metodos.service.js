// Initializes the `metodos` service on path `/metodos`
const { Metodos } = require('./metodos.class');
const createModel = require('../../models/metodos.model');
const hooks = require('./metodos.hooks');

const logger = require('../../logger');
const { QueryTypes } = require("sequelize");

const memory = require('feathers-memory');
const swagger = require('feathers-swagger');

module.exports = function (app) {
  const options = {
    // Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  // app.use('/metodos', new Metodos(options, app));

  // Get our initialized service so that we can register hooks
  // const service = app.service('metodos');
  // service.hooks(hooks);

  const sequelizeClient = app.get("sequelizeClient");

  //SP OBTIENE CONSOLIDADO CANTIDAD PERSONAS CON DISCAPACIDAD SEXO RANGO DE EDADES POR UBIGEO
  // service generation
  // const messageService1 = memory();
  // messageService1.docs = {
  //   description: 'Obtener cantidad de personas sexo y rango de edades por ubigeo',
  //   find: {
  //     description: 'Obtener cantidad de personas sexo y rango de edades por ubigeo',
  //   },
  //   operations: {
  //     find: false,
  //     get: false,
  //     update: false,
  //     patch: false,
  //     remove: false
  //   },
  //   definition: {
  //     type: 'object',
  //     required: [
  //       'codubigeo'
  //     ],
  //     properties: {
  //       codubigeo: {
  //         type: 'string',
  //         description: 'Código de Ubigeo'
  //       }
  //       // ,userId: {
  //       //   type: 'string',
  //       //   description: 'The id of the user that send the message'
  //       // }
  //     }
  //   }
  // };

  app.use(
    "/getcantpersexorangoedadesxubigeo",
    new Metodos(options, app),
    // messageService1,
    async (req, res) => {
      //console.log('Request Type:', req.method);
      // console.log(req.body);
      const _codubigeo = req.body.codubigeo;
      const records = await sequelizeClient
        .query("exec sp_PUD_get_personas_sexo_rangoedad :codubigeo;", {
          replacements: { codubigeo: _codubigeo },
          type: QueryTypes.SELECT,
        })
        .then(function (response) {
          // console.log(response);
          logger.info('call method getcantpersexorangoedadesxubigeo - exitoso');
          const resp = {
            succes: true,
            data: response
          }

          res.json(resp);
        })
        .catch(function (err) {
          res.json(err);
          logger.error(err);
        });
    }
  );

  // Get our initialized service so that we can register hooks
  const metodo1 = app.service("getcantpersexorangoedadesxubigeo");
  metodo1.hooks(hooks);

  //SP OBTIENE CONSOLIDADO CANTIDAD PERSONAS CON DISCAPACIDAD SEXO RANGO DE EDADES POR UBIGEO
  // service generation
  // const messageService2 = memory();
  // messageService2.docs = {
  //   description: 'Obtener cantidad de personas sexo y rango de edades por ubigeo y rango de edades',
  //   find: {
  //     description: 'Obtener cantidad de personas sexo y rango de edades por ubigeo y rango de edades',
  //   },
  //   operations: {
  //     find: false,
  //     get: false,
  //     update: false,
  //     patch: false,
  //     remove: false
  //   },
  //   definition: {
  //     type: 'object',
  //     required: [
  //       'codubigeo',
  //       'edadmin',
  //       'edadmax'
  //     ],
  //     properties: {
  //       codubigeo: {
  //         type: 'string',
  //         description: 'Código de Ubigeo'
  //       },
  //       edadmin: {
  //         type: 'integer',
  //         description: 'Edad minima'
  //       },
  //       edadmax: {
  //         type: 'integer',
  //         description: 'Edad máxima'
  //       }
  //     }
  //   }
  // };
  app.use(
    "/getcantpersexorangoedadesxubigeoxedades",
    new Metodos(options, app),
    // messageService2,
    async (req, res) => {
      //console.log('Request Type:', req.method);
      // console.log(req.body);
      const _codubigeo = req.body.codubigeo;
      const _edadmin = parseInt(req.body.edadmin);
      const _edadmax = parseInt(req.body.edadmax);
      const records = await sequelizeClient
        .query("exec sp_PUD_get_personas_sexo_rangoedad_x_ubigeo_edades :codubigeo,:edadmin,:edadmax;", {
          replacements: { codubigeo: _codubigeo, edadmin: _edadmin, edadmax: _edadmax },
          type: QueryTypes.SELECT,
        })
        .then(function (response) {
          // console.log(response);
          logger.info('call method getcantpersexorangoedadesxubigeoxedades - exitoso');
          const resp = {
            succes: true,
            data: response
          }

          res.json(resp);
        })
        .catch(function (err) {
          res.json(err);
          logger.error(err);
        });
    }
  );
  // Get our initialized service so that we can register hooks
  const metodo2 = app.service("getcantpersexorangoedadesxubigeoxedades");
  metodo2.hooks(hooks);

  
  //SP OBTIENE CONSOLIDADO CANTIDAD PERSONAS CON DISCAPACIDAD SEXO RANGO DE EDADES POR UBIGEO
  // service generation
  // const messageService3 = memory();
  // messageService3.docs = {
  //   description: 'Obtener cantidad de personas nivel de gravedad, sexo y rango de edades por ubigeo, rango de edades y sexo',
  //   find: {
  //     description: 'Obtener cantidad de personas nivel de gravedad, sexo y rango de edades por ubigeo, rango de edades y sexo',
  //   },
  //   operations: {
  //     find: false,
  //     get: false,
  //     update: false,
  //     patch: false,
  //     remove: false
  //   },
  //   definition: {
  //     type: 'object',
  //     required: [
  //       'codubigeo',
  //       'edadmin',
  //       'edadmax',
  //       'sexo'
  //     ],
  //     properties: {
  //       codubigeo: {
  //         type: 'string',
  //         description: 'Código de Ubigeo'
  //       },
  //       edadmin: {
  //         type: 'integer',
  //         description: 'Edad minima'
  //       },
  //       edadmax: {
  //         type: 'integer',
  //         description: 'Edad máxima'
  //       },
  //       sexo: {
  //         type: 'string',
  //         description: 'Sexo: "", F o M'
  //       }
  //     }
  //   }
  // };
  app.use(
    "/getcantpernivelgravedadsexorangoedadesxubigeoxedades",
    new Metodos(options, app),
    // messageService3,
    async (req, res) => {
      //console.log('Request Type:', req.method);
      // console.log(req.body);
      const _codubigeo = req.body.codubigeo;
      const _edadmin = parseInt(req.body.edadmin);
      const _edadmax = parseInt(req.body.edadmax);
      const _sexo = req.body.sexo;
      const records = await sequelizeClient
        .query("exec sp_PUD_get_personas_nivelgravedad_x_ubigeo_edades :codubigeo,:edadmin,:edadmax,:sexo;", {
          replacements: { codubigeo: _codubigeo, edadmin: _edadmin, edadmax: _edadmax, sexo: _sexo },
          type: QueryTypes.SELECT,
        })
        .then(function (response) {
          // console.log(response);
          logger.info('call method getcantpernivelgravedadsexorangoedadesxubigeoxedades - exitoso');
          const resp = {
            succes: true,
            data: response
          }

          res.json(resp);
        })
        .catch(function (err) {
          res.json(err);
          logger.error(err);
        });
    }
  );
  // Get our initialized service so that we can register hooks
  const metodo3 = app.service("getcantpernivelgravedadsexorangoedadesxubigeoxedades");
  metodo3.hooks(hooks);


  //SP OBTIENE DISTRIBUCIÓN DE AFILIADOS
  // service generation
  // const messageService4 = memory();
  // messageService4.docs = {
  //   description: 'Obtener distribucion de afiliados por ubigeo',
  //   find: {
  //     description: 'Obtener distribucion de afiliados por ubigeo',
  //   },
  //   operations: {
  //     find: false,
  //     get: false,
  //     update: false,
  //     patch: false,
  //     remove: false
  //   },
  //   definition: {
  //     type: 'object',
  //     required: [
  //       'codubigeo'
  //     ],
  //     properties: {
  //       codubigeo: {
  //         type: 'string',
  //         description: 'Código de Ubigeo'
  //       }
  //     }
  //   }
  // };

  app.use(
    "/getdistribucionafiliadoxubigeo",
    new Metodos(options, app),
    // messageService4,
    async (req, res) => {
      //console.log('Request Type:', req.method);
      // console.log(req.body);
      const _codubigeo = req.body.codubigeo;
      const records = await sequelizeClient
        .query("exec sp_PUD_get_distribucion_afiliados :codubigeo;", {
          replacements: { codubigeo: _codubigeo },
          type: QueryTypes.SELECT,
        })
        .then(function (response) {
          // console.log(response);
          logger.info('call method getdistribucionafiliadoxubigeo - exitoso');
          const resp = {
            succes: true,
            data: response
          }

          res.json(resp);
        })
        .catch(function (err) {
          res.json(err);
          logger.error(err);
        });
    }
  );
  // Get our initialized service so that we can register hooks
  const metodo4 = app.service("getdistribucionafiliadoxubigeo");
  metodo4.hooks(hooks);


  //#region Get Departamentos Ubigeo
  app.use(
    "/getubigeodepartamentos",
    new Metodos(options, app),
    // messageService4,
    async (req, res) => {
      //console.log('Request Type:', req.method);
      // console.log(req.body);
      // const _codubigeo = req.body.codubigeo;
      const records = await sequelizeClient
        .query("exec sp_PUD_get_ubgigeo_departamentos;", {
          type: QueryTypes.SELECT,
        })
        .then(function (response) {
          // console.log(response);
          logger.info('call method sp_PUD_get_ubgigeo_departamentos - exitoso');
          const resp = {
            succes: true,
            data: response
          }

          res.json(resp);
        })
        .catch(function (err) {
          res.json(err);
          logger.error(err);
        });
    }
  );
  // Get our initialized service so that we can register hooks
  const metodo5 = app.service("getubigeodepartamentos");
  metodo5.hooks(hooks);
  //#endregion

  //#region Get provincia
  app.use(
    "/getubigeoprovincias",
    new Metodos(options, app),
    // messageService4,
    async (req, res) => {
      //console.log('Request Type:', req.method);
      // console.log(req.body);
      const _codubigeo = req.body.codubigeo;
      const records = await sequelizeClient
        .query("exec sp_PUD_get_ubgigeo_provincias :codubigeo;", {
          replacements: { codubigeo: _codubigeo },
          type: QueryTypes.SELECT,
        })
        .then(function (response) {
          // console.log(response);
          logger.info('call method sp_PUD_get_ubgigeo_provincias - exitoso');
          const resp = {
            succes: true,
            data: response
          }

          res.json(resp);
        })
        .catch(function (err) {
          res.json(err);
          logger.error(err);
        });
    }
  );
  // Get our initialized service so that we can register hooks
  const metodo6 = app.service("getubigeoprovincias");
  metodo6.hooks(hooks);
  //#endregion

  //#region Get distritos
  app.use(
    "/getubigeodistritos",
    new Metodos(options, app),
    async (req, res) => {
      const _codubigeo = req.body.codubigeo;
      const records = await sequelizeClient
        .query("exec sp_PUD_get_ubgigeo_distritos :codubigeo;", {
          replacements: { codubigeo: _codubigeo },
          type: QueryTypes.SELECT,
        })
        .then(function (response) {
          // console.log(response);
          logger.info('call method sp_PUD_get_ubgigeo_distritos - exitoso');
          const resp = {
            succes: true,
            data: response
          }

          res.json(resp);
        })
        .catch(function (err) {
          res.json(err);
          logger.error(err);
        });
    }
  );
  // Get our initialized service so that we can register hooks
  const metodo7 = app.service("getubigeodistritos");
  metodo7.hooks(hooks);
  //#endregion

  //#region Get historico de afiliados por institucion
  app.use(
    "/gethistoricoafiliadosinstitucionfechas",
    new Metodos(options, app),
    async (req, res) => {
      const _codubigeo = req.body.codubigeo;
      const _fecInicio = req.body.fechainicio;
      const _fecFin = req.body.fechafin;
      // console.log(_fecInicio)
      // console.log(_fecFin)
      const records = await sequelizeClient
        .query("exec sp_PUD_get_historico_distribucion_afiliados :codubigeo,:fecInicio,:fecFin ;", {
          replacements: { codubigeo: _codubigeo, fecInicio: _fecInicio, fecFin: _fecFin },
          type: QueryTypes.SELECT,
        })
        .then(function (response) {
          // console.log(response);
          logger.info('call method sp_PUD_get_historico_distribucion_afiliados - exitoso');
          const resp = {
            succes: true,
            data: response
          }

          res.json(resp);
        })
        .catch(function (err) {
          res.json(err);
          logger.error(err);
        });
    }
  );
  // Get our initialized service so that we can register hooks
  const metodo8 = app.service("gethistoricoafiliadosinstitucionfechas");
  metodo8.hooks(hooks);
  //#endregion

};
