const { authenticate } = require('@feathersjs/authentication').hooks;
const allowApiKey = require("./../../hooks/allowApiKey");

module.exports = {
  before: {
    all: [ allowApiKey(), authenticate('apiKey') ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
